package ru.t1.nkiryukhin.tm.service;

import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.migration.AbstractSchemeTest;

@Category(UnitCategory.class)
public final class UserServiceTest extends AbstractSchemeTest {
/*

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private static final IUserService service = new UserService(connectionService, propertyService, taskService, projectService, sessionService);

    @NotNull
    private static final Liquibase liquibase = liquibase("changelog/changelog-master.xml");


    @BeforeClass
    public static void setUp() throws LiquibaseException {
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @AfterClass
    public static void tearDown() throws AbstractException {
        connectionService.close();
    }

    @Before
    public void loadTestData() {
        service.add(USUAL_USER);
    }

    @After
    public void removeTestData() throws AbstractException {
        service.remove(USER_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(service.add(NULL_USER));
        Assert.assertNotNull(service.add(ADMIN_USER));
        @Nullable final User user = service.findOneById(ADMIN_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER, user);
    }

    @Test
    public void findByEmail() throws EmailEmptyException {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.findByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.findByEmail("");
        });
        String email = USUAL_USER.getEmail();
        @Nullable final User user = service.findByEmail(email);
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void existsById() throws AbstractFieldException {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(service.existsById(USUAL_USER.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertNull(service.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = service.findOneById(USUAL_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void removeById() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        service.add(ADMIN_USER);
        Assert.assertTrue(service.existsById(ADMIN_USER.getId()));
        service.removeById(ADMIN_USER.getId());
        Assert.assertFalse(service.existsById(ADMIN_USER.getId()));
    }

    @Test
    public void getSize() {
        int initCount = service.getSize();
        service.add(ADMIN_USER);
        Assert.assertEquals(service.getSize(), initCount + 1);
    }

    @Test
    public void removeAll() throws AbstractException {
        int initCount = service.getSize();
        service.add(ADMIN_USER);
        service.add(ADMIN_USER_2);
        service.remove(ADMIN_USER_LIST);
        Assert.assertEquals(0, service.getSize() - initCount);
    }

    @Test
    public void create() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USUAL_USER_LOGIN, ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, "");
        });
        @NotNull final User user = service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_USER_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), user.getPasswordHash());
        service.removeOne(user);
    }

    @Test
    public void createWithEmail() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USUAL_USER_LOGIN, ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, null, ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, "", ADMIN_USER_EMAIL);
        });
        Assert.assertThrows(ExistsEmailException.class, () -> {
            service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, USUAL_USER_EMAIL);
        });
        @NotNull final User user = service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, ADMIN_USER_EMAIL);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_USER_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_USER_EMAIL, user.getEmail());
        service.removeOne(user);
    }

    @Test
    public void createWithRole() throws AbstractException {
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_USER_PASSWORD, role);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_USER_PASSWORD, role);
        });
        Assert.assertThrows(ExistsLoginException.class, () -> {
            service.create(USUAL_USER_LOGIN, ADMIN_USER_PASSWORD, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, null, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_USER_LOGIN, "", role);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @NotNull final Role nullRole = null;
            service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, nullRole);
        });
        @NotNull final User user = service.create(ADMIN_USER_LOGIN, ADMIN_USER_PASSWORD, Role.ADMIN);
        Assert.assertEquals(user, service.findOneById(user.getId()));
        Assert.assertEquals(ADMIN_USER_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
        service.removeOne(user);
    }

    @Test
    public void findByLogin() throws LoginEmptyException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin("");
        });
        @Nullable final User user = service.findByLogin(USUAL_USER_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void remove() throws AbstractException {
        @Nullable final User createdUser = service.add(ADMIN_USER);
        projectService.add(ADMIN_PROJECT1);
        taskService.add(ADMIN_TASK1);
        service.removeById(createdUser.getId());
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
        Assert.assertNull(projectService.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
        Assert.assertNull(taskService.findOneById(ADMIN_USER.getId(), ADMIN_TASK1.getId()));
    }

    @Test
    public void removeByEmail() throws AbstractException {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.removeByEmail(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_USER);
        service.removeByEmail(ADMIN_USER_EMAIL);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void removeByLogin() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin("");
        });
        service.add(ADMIN_USER);
        service.removeByLogin(ADMIN_USER_LOGIN);
        Assert.assertNull(service.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void setPassword() throws AbstractException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword(null, ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword("", ADMIN_USER_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.setPassword(NON_EXISTING_USER_ID, ADMIN_USER_PASSWORD);
        });
        service.setPassword(USUAL_USER.getId(), ADMIN_USER_PASSWORD);
        User usualUser = service.findOneById(USUAL_USER.getId());
        Assert.assertEquals(ADMIN_USER.getPasswordHash(), usualUser.getPasswordHash());
        service.setPassword(USUAL_USER.getId(), USUAL_USER_PASSWORD);
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(service.isLoginExist(null));
        Assert.assertFalse(service.isLoginExist(""));
        Assert.assertTrue(service.isLoginExist(USUAL_USER_LOGIN));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(service.isEmailExist(null));
        Assert.assertFalse(service.isEmailExist(""));
        String email = USUAL_USER.getEmail();
        Assert.assertTrue(service.isEmailExist(email));
    }

    @Test
    public void lockUserByLogin() throws AbstractException {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.lockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.add(ADMIN_USER);
        service.lockUserByLogin(ADMIN_USER_LOGIN);
        User adminUser = service.findByLogin(ADMIN_USER_LOGIN);
        Assert.assertTrue(adminUser.getLocked());
        service.unlockUserByLogin(ADMIN_USER_LOGIN);
        adminUser = service.findByLogin(ADMIN_USER_LOGIN);
        Assert.assertFalse(adminUser.getLocked());
    }

    @Test
    public void updateUser() throws AbstractException {
        @NotNull final String firstName = "firstName";
        @NotNull final String lastName = "lastName";
        @NotNull final String middleName = "middleName";
        @NotNull final String email = "email";
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser(null, firstName, lastName, middleName, email);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser("", firstName, lastName, middleName, email);
        });
        service.updateUser(USUAL_USER.getId(), firstName, lastName, middleName, email);
        User updatedUser = service.findOneById(USUAL_USER.getId());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }
*/

}

