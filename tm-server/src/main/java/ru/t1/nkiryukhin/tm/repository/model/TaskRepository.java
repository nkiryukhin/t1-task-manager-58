package ru.t1.nkiryukhin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.api.repository.model.ITaskRepository;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Task.class).setParameter("userId", userId).getResultList();
    }

    @Override
    public List<Task> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, Task.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) return 0;
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class).setParameter("userId", userId).getSingleResult().intValue();
    }

}