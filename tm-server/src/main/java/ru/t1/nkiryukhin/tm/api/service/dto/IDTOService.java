package ru.t1.nkiryukhin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.AbstractModelDTO;

public interface IDTOService<M extends AbstractModelDTO> {

    @Nullable
    M add(@Nullable M model);

    void removeOne(@Nullable M model);

    void update(@NotNull M model);

}