package ru.t1.nkiryukhin.tm.dataDTO;

import org.jetbrains.annotations.NotNull;

public class TaskTestData {

    @NotNull
    public final static String TASK_ONE_NAME = "Task 1";

    @NotNull
    public final static String TASK_ONE_DESCRIPTION = "Task 1 Desc";

    @NotNull
    public final static String TASK_TWO_NAME = "Task 2";

    @NotNull
    public final static String TASK_TWO_DESCRIPTION = "Task 2 Desc";

    @NotNull
    public final static String TASK_THREE_NAME = "Task 3";

    @NotNull
    public final static String TASK_THREE_DESCRIPTION = "Task 3 Desc";

}