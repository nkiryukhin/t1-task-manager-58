package ru.t1.nkiryukhin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.request.TaskClearRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    @EventListener(condition = "@taskClearListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        taskEndpoint.clearTask(request);
    }

}
